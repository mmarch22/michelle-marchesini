all: hello_world

hello_world: hello_world.c
	gcc -Wall -o hello_world hello_world.c

clean:
	rm -f hello_world
