# README
Michelle Marchesini Vanegas (She/Her) <p/>
Computer Science and Pre-Engineering Major

## CS Fundamentals
* [Command line utilities and Bash](https://code.cs.earlham.edu/mmarch22/michelle-marchesini/-/blob/main/bashref.md)
* [Introduction to regular expressions](https://code.cs.earlham.edu/mmarch22/michelle-marchesini/-/blob/main/bashref.md)
* [Version control with Git](https://code.cs.earlham.edu/mmarch22/michelle-marchesini/-/blob/main/.gitignore)
* [Markdown for composing readable text](https://code.cs.earlham.edu/mmarch22/michelle-marchesini/-/blob/main/README.md?ref_type=heads)
* [C programming language](https://code.cs.earlham.edu/mmarch22/michelle-marchesini/-/blob/main/makefile?ref_type=heads)
* [Building and installing software](https://code.cs.earlham.edu/mmarch22/michelle-marchesini/-/tree/main/C?ref_type=heads)
* [Batch computing and scheduling](https://code.cs.earlham.edu/mmarch22/michelle-marchesini/-/tree/main/Batch%20computing%20and%20scheduling)

## Skills
- [Skills Course](4d10c007e3becf2461c45a9f3eaa44d12505315c)

## Link
* [Moodle](https://moodle.earlham.edu/course/view.php?id=886/)

## Topical Units
- [Programming Environments - R](https://code.cs.earlham.edu/mmarch22/michelle-marchesini/-/tree/main/R?ref_type=heads)
- [Cybersecurity](6a5542300e984e77d0cb9981d04fe569ed622a5d)
- [C](73eff3d5ae4c954ed98c2f67af2299e2f5af5dee)
- [JAVA](231930f2b115277b77ae5c77d008a69af5a02197)
