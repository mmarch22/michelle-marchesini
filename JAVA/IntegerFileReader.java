import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class IntegerFileReader {

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: java IntegerFileReader input-file-name");
            System.exit(1);
        }

        String fileName = args[0];
        try {
            int[] values = readIntegersFromFile(fileName);
            if (values.length > 0) {
                int count = values.length;
                long total = 0;
                int maxInt = Integer.MIN_VALUE;
                for (int value : values) {
                    total += value;
                    if (value > maxInt) {
                        maxInt = value;
                    }
                }
                double average = (double) total / count;
                displayResults(count, total, average, maxInt);
            } else {
                System.err.println("The file is empty.");
            }
        } catch (IOException e) {
            System.err.println("Error reading from file: " + e.getMessage());
        }
    }

    private static int[] readIntegersFromFile(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line;
        int[] values = new int[1000]; // assuming max 1000 integers
        int count = 0;
        while ((line = reader.readLine()) != null) {
            values[count++] = Integer.parseInt(line);
        }
        reader.close();
        int[] trimmedValues = new int[count];
        System.arraycopy(values, 0, trimmedValues, 0, count);
        return trimmedValues;
    }

    private static void displayResults(int count, long total, double average, int maxInt) {
        System.err.println("Count: " + count);
        System.err.println("Total: " + total);
        System.err.println("Average: " + average);
        System.err.println("Maximum Integer: " + maxInt);
    }
}
