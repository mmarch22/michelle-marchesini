# Economic and Racial Inequality in the United States

The median wage is the hourly wage in the middle of the wage distribution; 50 percent of wage earners earn less and 50 percent earn more. The average wage is the arithmetic mean of hourly wages; or, the sum of all workers’ hourly wages divided by the number of workers. (Source: opi.org)

This graph aims to illustrate the economic inequality between different racial groups in the United States based on their hourly wages. 

![Alt Text](R/graphs/wagesbyrace.png)

<hr>

### Resources

* [Raw Data](ba2f9193c3d3fd61b2f25ee0e6e9a11d2861549d)
* [R code](45c463ebe749fa4f7407251be76f0173f96ad5a0)
* [Source](https://www.epi.org/data/#?subject=wage-avg&r=*)
