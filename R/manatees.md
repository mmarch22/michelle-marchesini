# Manatees Population over Time

Manatees are large, fully aquatic, mostly herbivorous marine mammals sometimes known as sea cows. The main causes of death for manatees are human-related issues, such as habitat destruction and human objects. Their slow-moving, curious nature has led to violent collisions with propeller-driven boats and ships. Some manatees have been found with over 50 scars on them from propeller blades.

This graph analyses the correlation between the number of boats in a determined area and the casualties in the manatee population in 18 years.

![Alt Text](R/graphs/manatees.png)

### Resources

-[Raw Data](6577efa511ece08636f21166fe10fac2d7e0c847)

-[R code](03a02d8c091a020f731051bac9acb08ce6b9d1da)

