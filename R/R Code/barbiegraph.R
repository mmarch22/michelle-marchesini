install.packages("ggplot2")
library(ggplot2)

data <- read.csv("multiTimeline.edited.csv")

data$Date <- as.Date(data$Date)

ggplot(data, aes(x = Date, y = Times)) +
  geom_line(color = "pink") + 
  labs(title = "'Barbie' trending over time",
       x = "Date",
       y = "Interest",
       caption = "Data source: Google Trends")

ggsave("barbie_trend.png", plot, width = 8, height = 6)
