install.packages("ggplot2")
library(ggplot2)

# Read data from CSV file
data <- read.csv("us-states.csv")

# Define a vector of Midwest states
midwest_states <- c("Illinois", "Indiana", "Iowa", "Kansas", "Michigan", "Minnesota", "Missouri", "Nebraska", "North Dakota", "Ohio", "South Dakota", "Wisconsin")

#  Filter data to include only Midwest states
midwest_data <- data[data$state %in% midwest_states, ]

# Aggregate cases by state
midwest_data_sum <- aggregate(cases ~ state, midwest_data, sum)

# Create bar graph
ggplot(midwest_data_sum, aes(x = state, y = cases)) +
  geom_bar(stat = "identity", fill = "skyblue") +
  labs(title = "Total COVID-19 Cases in Midwestern States",
       x = "state",
       y = "Total Number of Cases",
       caption = "Wages in the USA. (January 2020-March 2023). Data source: The New York Times")
