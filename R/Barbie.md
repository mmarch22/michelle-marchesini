# Popularity Analysis


The google tool 'Trends' analizes and monitors Google searches and their popularity. In this case, I researched the popularity of the word search '_Barbie_' from March 3rd, 2023 to March 3rd, 2024.

![Alt Text](R/graphs/barbie.png)

In the graph, we can observe that 'Barbie' reached its popularity peak in July 2023. The movie premiered in theatres on July 23rd, 2023, breaking records and creating a cultural and social phenomenon in the media industry.

### Resources

-[Raw Data](1d2708239ba7d32d4f275810d8da41383b0edd3d)

-[R code](https://code.cs.earlham.edu/mmarch22/michelle-marchesini/-/blob/6cbc257dd8115e68d8faa85055df7a7aca12b2d9/R/barbiegraph.R)
