# Covid-19 Cases in the United States (Midwest)

The following graph compiles and illustrate the number of COVID-19 cases in the Midwest (Illinois, Indiana, Iowa, Kansas, Michigan, Minnesota, Missouri, Nebraska, North Dakota, Ohio, South Dakota, Wisconsin) from January 2020 to March 2023. The raw data contains a list of all reported COVID-19 in the country and filters them by states to then sum them and graph the results.

![Alt Text](R/graphs/covid.png)

<hr>

* [Raw Data](https://github.com/nytimes/covid-19-data?tab=readme-ov-file)

* [R code](1588128997f711cb2245e7a1aaf3f9c104b25bfd)

* [Source](https://github.com/nytimes/covid-19-data?tab=readme-ov-file)
