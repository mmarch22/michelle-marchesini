- Social engineering is the psychological act of manipulating people into performing actions or divulging confidential information to obtain a personal benefit

- Ethical hacking is the use of hacking techniques to secure and improve the technology of organizations

- a Firewall is a network security system that monitors and controls incoming and outgoing network traffic based on predetermined security rules

- Phishing is where a form of scam where hackers deceive their victims to reveal personal and sensitive information

- Ransomware is a type of malware that holds the victims data or device hostage or locked until the victim pays the ransom

- Encryption is the process of encoding information and files. It can be used to protect data form being stolen, changed or compromised.

- Two-Factor Authentication is the extra step of authentication to ensure the access to a website or application is reliable and safe

- Buffer Overflow is when the amount of data in a buffer exceeds its storage capacity

- Integer Overflow is a vulnerability that lets a malicious hacker trick the program into performing an integer operation whose result exceeds the allocated memory space. 

- Code injection vulnerabilities are attacks that inject code into an application. That injected code is then interpreted by the application, changing the way a program executes

- Race condition vulnerabilities occurs when the timing or sequence of events in a multithreaded or asynchronous system can be manipulated by attackers to compromise security.

- Privilege confusion is a security issue where an entity that doesn't have permission to perform an action can coerce a more-privileged entity to perform the action.

- A password manager is a technology tool that helps internet users create, save, manage and use passwords across different online services.

