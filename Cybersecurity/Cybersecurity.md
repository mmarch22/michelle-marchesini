Route to it
traceroute to 157.150.185.49 (157.150.185.49), 30 hops max, 60 byte packets

 1  159.28.22.254 (159.28.22.254)  4.214 ms  4.519 ms  4.858 ms

 2  159.28.31.70 (159.28.31.70)  0.452 ms  0.466 ms  0.411 ms

 3  159.28.0.1 (159.28.0.1)  1.669 ms  1.538 ms  1.541 ms

 4  199.8.48.65 (199.8.48.65)  4.403 ms  4.294 ms  4.419 ms

 5  ae-0.2021.rtr.ll.indiana.gigapop.net (199.8.220.5)  4.539 ms  4.432 ms  4.312 ms

 6  ipls-b2-link.ip.twelve99.net (62.115.162.76)  5.152 ms  4.840 ms  4.881 ms

 7  atl-b24-link.ip.twelve99.net (62.115.119.200)  21.322 ms  14.726 ms  14.648 ms

 8  atl-bb2-link.ip.twelve99.net (62.115.143.236)  14.970 ms ash-bb2-link.ip.twelve99.ne (62.115.125.129)  29.625 ms 
 
atl-bb2-link.ip.twelve99.net (62.115.143.236)  15.303 ms

 9  * * ash-b2-link.ip.twelve99.net (62.115.123.125)  26.389 ms

10  ash-b2-link.ip.twelve99.net (62.115.123.125)  26.877 ms  26.237 ms  26.220 ms

11  orange-ic-369583.ip.twelve99-cust.net (195.12.254.103)  27.629 ms * *

12  * * *
13  193.251.251.196 (193.251.251.196)  27.926 ms  27.908 ms  25.014 ms

14  * * *

15  * * *

16  * * *

17  * * *

18  * * *

19  * * *

20  * * *

21  * * *

22  * * *

23  * * *

24  * * *

25  * * *

26  * * *

27  * * *

28  * * *

29  * * *

30  * * *

#
-**DNS Sever/Name**

nslookup 157.150.185.49
49.185.150.157.in-addr.arpa	name = www.un.org.

Authoritative answers can be found from:
#
- **Who owns it?**

 ARIN WHOIS data and services are subject to the Terms of Use
/# available at: https://www.arin.net/resources/registry/whois/tou/

 If you see inaccuracies in the results, please report at
 https://www.arin.net/resources/registry/whois/inaccuracy_reporting/

Copyright 1997-2024, American Registry for Internet Numbers, Ltd.

#


NetRange:       157.150.0.0 - 157.150.255.255

CIDR:           157.150.0.0/16

NetName:        UN-NET

NetHandle:      NET-157-150-0-0-1

Parent:         NET157 (NET-157-0-0-0-0)

NetType:        Direct Allocation

OriginAS:       AS22723

Organization:   United Nations (UNITED-2)

RegDate:        1992-02-14

Updated:        2021-12-14

Ref:            https://rdap.arin.net/registry/ip/157.150.0.0



OrgName:        United Nations\
OrgId:          UNITED-2 \
Address:        United Nations secretariat \
Address:        405 East 42nd St \
City:           New York \
StateProv:      NY\
PostalCode:     10017\
Country:        US\
RegDate:        1992-02-14\
Updated:        2017-01-28\
Ref:            https://rdap.arin.net/registry/entity/UNITED-2\


OrgTechHandle: AJL5-ARIN\
OrgTechName:   Linehan, Andrew John\
OrgTechPhone:  +1-347-749-1156\
OrgTechEmail:  linehan@un.org\
OrgTechRef:    https://rdap.arin.net/registry/entity/AJL5-ARIN\

OrgAbuseHandle: DEBAR-ARIN\
OrgAbuseName:   Debargue, Olivier \
OrgAbusePhone:  +1-917-367-0765 \
OrgAbuseEmail:  debargue@un.org\
OrgAbuseRef:    https://rdap.arin.net/registry/entity/DEBAR-ARIN\

OrgAbuseHandle: AJL5-ARIN\
OrgAbuseName:   Linehan, Andrew John\
OrgAbusePhone:  +1-347-749-1156 \
OrgAbuseEmail:  linehan@un.org\
OrgAbuseRef:    https://rdap.arin.net/registry/entity/AJL5-ARIN\

OrgTechHandle: DEBAR-ARIN\
OrgTechName:   Debargue, Olivier \
OrgTechPhone:  +1-917-367-0765 \
OrgTechEmail:  debargue@un.org\
OrgTechRef:    https://rdap.arin.net/registry/entity/DEBAR-ARIN\

RTechHandle: DEBAR-ARIN\
RTechName:   Debargue, Olivier \
RTechPhone:  +1-917-367-0765 \
RTechEmail:  debargue@un.org\
RTechRef:    https://rdap.arin.net/registry/entity/DEBAR-ARIN\


\# ARIN WHOIS data and services are subject to the Terms of Use
\# available at: https://www.arin.net/resources/registry/whois/tou/
\#
\# If you see inaccuracies in the results, please report at
\# https://www.arin.net/resources/registry/whois/inaccuracy_reporting/
\#
\# Copyright 1997-2024, American Registry for Internet Numbers, Ltd.
\#

30  * * *
