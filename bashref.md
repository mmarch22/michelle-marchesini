## Bash Reference
**pwd** - print work directory.

**ls** - list files in current directory.

**ls -l** - all files.

**ls -t** - sort files by time.

**ls -a** - all files.

**ls -F -l** - list all files with an indicator

**ls -r** - sort files by reverse order (recent at bottom).

**cd** - change directory.

**cd ..** - go back to one directory.

**cd ../../** - go to root node.

**cd~** - back to home.

**cd -** - back to last one.

**cd + TAB** - complete the next.

**exit** - exit your shell (in the system).

**mkdir** - make directory.

## Terminal Commands 
**exit** - Exit the current shell or terminal session.

**Ctrl + C** - Interrupt (kill) the current foreground process.

**Ctrl + D** - End of file or exit the shell.

**Ctrl + Z** - Suspend the current foreground process.

**bg** - Move a suspended process to the background.

**fg** - Bring a background process to the foreground.

**jobs** - List active jobs.

**kill** - Send a signal to a process (e.g., kill PID).

**halt** - Stop the system.

**clear** - Clear the terminal.


## File Manipulation

**c** - copy files or directories.

**mv** - move or rename files or directories.

**cat** - concatenate and display files.

**grep** - search for patterns in files.

**echo** - display text or variables.

**diff** - compare files line by line.

**rm** - remove files or directories.

**rmdir** - Remove empty directories.

**find** - Search for files in a directory hierarchy.

**cat** - concatenate, prints into the shell.

**cat** first.dat second.dat (concatenates first and second).

**wc** - word count.

**wc -l** - counts lines in a file.

**nano filename.txt** - edit text file in the shell interface.

**cat myfile.txt | grep "hello"** - searches myfile.txt for the string "hello".

**head filename.txt** - show beginning of the file.

**tail filename.txt**- show ending of the file.


## Patterns and Commands

**.**	replaces any character

**^**	matches start of string

**$**	matches end of string

**"*"**	matches up zero or more times the preceding character

**\**	Represent special characters

**()**	Groups regular expressions

**?**	Matches up exactly one character

**grep a** -> search for files with the letter 'a'

**grep ^a** -> search for files beginning with the letter 'a'

**grep $a** -> search for files ending with the letter 'a'

**grep -a p\{x}** -> search for files with the letter 'a' exactly x amount of times

**grep -a p\{x,z}** -> search for files with the letter 'a' between x and z times (inclusive)

**grep -a p\{x,}** -> search for files with the letter 'a' x amount of times or more

**grep "a\+t"** -> search for files with 't' preceding 'a'

**echo {a,f}** -> search for commands or a list of items, i.e., {a,b,c,d,e,f}

**grep 'word' sample** -> search for files containing 'word'

**egrep 'colou?r' sample** -> search for files containing 'color' or 'colour' (character before ? is optional)

**egrep '/.+/'** -> search the file for lines that contain one or more characters and output those lines

**egrep "gr[ae]y"** -> searches for both the word 'gray' and 'grey'

**sed 's/hello/goodbye/g'** -> replaces 'hello' with 'goodbye' globally

**sed '/error/d' logfile.txt** -> removes all lines containing the word "error" from 'logfile.txt'
